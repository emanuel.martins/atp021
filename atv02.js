const getPrimeNumber = (number) => {
    if (number <= 2) {
        return false;
    }

    for (let i = 2; i < number; i++) {
        if (number % i === 0) {
            return false;
        }
    }

    return true;
};

let numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for (let index = 0; index < numbers.length; index++) {
    if (getPrimeNumber(index)) {
        console.log(`Número Primo: ${index}`);
    }
}

numbers.forEach((number) =>
    number % 2 === 0 ? console.log(`Número Par: ${number}`) : null
);
