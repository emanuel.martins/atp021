const reader = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

const database = [];
let menuOption;

console.log(`
++++++++++++++ MENU  ++++++++++++++

.. 1 - Cadastrar;
.. 2 - Listar;
.. 3 - Atualizar;
.. 4 - Deletar;
.. 0 - Sair;
`);

const askForName = () => {
    let response;

    reader.question(`Nome: `, async (personName) => {
        response = personName;
        await reader.close();
    });

    return response;
};

const addPerson = (personName) => {
    database.push(personName);
};

const showDatabase = () => {
    console.table(database);
};

const updatePerson = (personName) => {
    let personIndex = database.indexOf(personName);

    database[personIndex] = personName;
};

const deletePerson = (personName) => {
    let personIndex = database.indexOf(personName);

    database.splice(personIndex, personIndex);
};

reader.question(`Escolha: `, async (option) => {
    let optionAndName = option.split(', ');

    menuOption = Number(optionAndName[0]);
    await reader.close();

    switch (menuOption) {
        case 1:
            addPerson(optionAndName[1]);
            showDatabase();
            break;

        case 2:
            showDatabase();
            break;

        case 3:
            updatePerson(optionAndName[1]);
            showDatabase();
            break;

        case 4:
            deletePerson(optionAndName[1]);
            showDatabase();
            break;
        default:
            break;
    }
});

addPerson('Pedro');
addPerson('Maria');
addPerson('Andrew');
addPerson('Livia');
addPerson('Ana');
addPerson('Pablo');
