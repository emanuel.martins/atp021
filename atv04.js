class Pessoa {
    #nome;
    #sobrenome;
    #idade;
    #cpf;
    #rg;

    constructor(nome, sobrenome, idade, cpf, rg) {
        this.setNome(nome);
        this.setSobrenome(sobrenome);
        this.setIdade(idade);
        this.setCpf(cpf);
        this.setRg(rg);
    }

    getNome() {
        return this.#nome;
    }

    setNome(nome) {
        this.#nome = nome;
    }

    getSobrenome() {
        return this.#sobrenome;
    }

    setSobrenome(sobrenome) {
        this.#sobrenome = sobrenome;
    }

    getIdade() {
        return this.#idade;
    }

    setIdade(idade) {
        this.#idade = idade;
    }

    getCpf() {
        return this.#cpf;
    }

    setCpf(cpf) {
        this.#cpf = cpf;
    }

    getRg() {
        return this.#rg;
    }

    setRg(rg) {
        this.#rg = rg;
    }
}

const pessoa1 = new Pessoa(
    'Emanuel',
    'Martins',
    21,
    '78945612300',
    '123456789'
);

const pessoa2 = new Pessoa('Victor', 'Scherer', 84, '78945612301', '123456782');

console.table([
    ['Nome', pessoa1.getNome()],
    ['Sobrenome', pessoa1.getSobrenome()],
    ['Idade', pessoa1.getIdade()],
    ['CPF', pessoa1.getCpf()],
    ['RG', pessoa1.getRg()],
]);

console.table([
    ['Nome', pessoa2.getNome()],
    ['Sobrenome', pessoa2.getSobrenome()],
    ['Idade', pessoa2.getIdade()],
    ['CPF', pessoa2.getCpf()],
    ['RG', pessoa2.getRg()],
]);
