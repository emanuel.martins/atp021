let funcionario = {
    nome: 'Emanuel',
    sobrenome: 'Martins',
    idade: 21,
    cpf: '74125369851',
    cargo: 'Auxiliar de Desenvolvimento',
    salario: 2000,
};

console.log(
    `${funcionario.cpf} ${funcionario.nome} ${funcionario.sobrenome}, ${funcionario.idade}, ${funcionario.cargo} ${funcionario.salario}`
);
